package com.dh.test.healthcheck.commands;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author mukhanov@gmail.com
 */
public class CommandResultsJsonSerializerTest {

    @Test
    public void testCommandResultJson() {

        CommandResult commandResult = new CommandResult("jasmin.com", "some output", LocalDateTime.now());

        Assert.assertEquals(commandResult.toJSON(),
                "{\"output\":\"some output\", \"timestamp\":\"" + DateTimeFormatter.ISO_DATE_TIME.format(commandResult.timestamp) + "\"}");
    }

    @Test
    public void testPingTCPCommandResultJson() {

        PingTCPCommandResult commandResult = new PingTCPCommandResult("jasmin.com", "some output", LocalDateTime.now(), 1000);

        Assert.assertEquals(commandResult.toJSON(),
                "{\"output\":\"some output\", \"timestamp\":\"" + DateTimeFormatter.ISO_DATE_TIME.format(commandResult.timestamp) + "\", \"responseTime\":" + commandResult.responseTime + "}");
    }

}