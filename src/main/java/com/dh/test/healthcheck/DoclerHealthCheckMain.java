package com.dh.test.healthcheck;

import com.dh.test.healthcheck.commands.CommandResult;
import com.dh.test.healthcheck.commands.PingCommand;
import com.dh.test.healthcheck.commands.PingTCPCommand;
import com.dh.test.healthcheck.commands.TraceRouteCommand;
import com.dh.test.healthcheck.config.HostConfig;
import com.dh.test.healthcheck.config.HostConfigLoader;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.LogManager;

/**
 * @author mukhanov@gmail.com
 */
public class DoclerHealthCheckMain {

    public static void main(String[] args) throws IOException {

        initLogger();

        List<HostConfig> hostConfigList = HostConfigLoader.loadConfigList();

        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(hostConfigList.size() * 3);

        for (HostConfig hostConfig : hostConfigList) {

            Map<String, CommandResult> commandStateMap = new ConcurrentHashMap<>(hostConfigList.size());

            executorService.scheduleWithFixedDelay(new PingCommand(hostConfig, commandStateMap)::run, 0, hostConfig.pingDelayMillis, TimeUnit.MILLISECONDS);
            executorService.scheduleWithFixedDelay(new PingTCPCommand(hostConfig, commandStateMap)::run, 0, hostConfig.tcpPingDelayMillis, TimeUnit.MILLISECONDS);
            executorService.scheduleWithFixedDelay(new TraceRouteCommand(hostConfig, commandStateMap)::run, 0, hostConfig.traceRouteDelayMillis, TimeUnit.MILLISECONDS);
        }

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                executorService.shutdown();
            }
        });

    }

    private static void initLogger() throws IOException {
        if (System.getProperty("java.util.logging.config.file") == null) {
            LogManager.getLogManager().readConfiguration(
                    DoclerHealthCheckMain.class
                            .getResourceAsStream("/logging.properties"));
        }
    }

}
