package com.dh.test.healthcheck.config;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * @author mukhanov@gmail.com
 */
public class HostConfigLoader {

    public static List<HostConfig> loadConfigList() throws IOException {

        Path healthCheckConfigPath = FileSystems.getDefault().getPath(".", "healhtcheck.properties");

        if (Files.notExists(healthCheckConfigPath)) {
            InputStream defaultConfigInputStream = Thread.currentThread()
                    .getContextClassLoader().getResourceAsStream("defaults.healhtcheck.properties");

            Files.copy(defaultConfigInputStream, healthCheckConfigPath);
        }


        Properties props = new Properties();

        props.load(Files.newBufferedReader(healthCheckConfigPath));

        return loadConfigInternal(props);
    }

    private static List<HostConfig> loadConfigInternal(Properties props) {
        String hostsStr = props.getProperty("hosts");

        if (hostsStr == null) {
            throw new IllegalStateException("hosts property is not defined");
        }

        String[] hosts = hostsStr.split(",");

        if (hosts.length == 0) {
            throw new IllegalStateException("There are no hosts configured");
        }

        if (!props.containsKey("tcpPingDelayMillis")) {
            throw new IllegalStateException("httpPingDelayMillis property is not defined");

        }

        Long tcpPingDelayMillis = getPropertyLong(props, "tcpPingDelayMillis");
        String tcpPingURL = getPropertyWithHostParameter(props, "tcpPingURL");

        String pingCommandTemplate = getPropertyWithHostParameter(props, "pingCommand");
        Long pingDelayMillis = getPropertyLong(props, "pingDelayMillis");

        String traceRouteCommandTemplate = getPropertyWithHostParameter(props, "traceRouteCommand");
        Long traceRouteDelayMillis = getPropertyLong(props, "traceRouteDelayMillis");


        String reportServerURL = props.getProperty("reportServerURL");

        if (reportServerURL == null) {
            throw new IllegalStateException("reportServerURL property is not defined");
        }

        List<HostConfig> hostConfigList = new ArrayList<>(hosts.length);

        for (String host : hosts) {

            hostConfigList.add(new HostConfig(
                    host,
                    reportServerURL,
                    tcpPingURL.replaceAll("\\$\\{host\\}", host),
                    tcpPingDelayMillis,
                    pingCommandTemplate.replaceAll("\\$\\{host\\}", host),
                    pingDelayMillis,
                    traceRouteCommandTemplate.replaceAll("\\$\\{host\\}", host),
                    traceRouteDelayMillis
            ));
        }

        return hostConfigList;

    }

    private static Long getPropertyLong(Properties props, String propertyName) {
        String propertyStr = props.getProperty(propertyName);
        Long propertyLong;
        try {
            propertyLong = Long.parseLong(propertyStr);
        } catch (NumberFormatException e) {
            throw new IllegalStateException("Unable to parse value for property [" + propertyName + "]");
        }
        return propertyLong;
    }

    private static String getPropertyWithHostParameter(Properties props, String commandProp) {
        String pingCommandTemplate = props.getProperty(commandProp);

        if (pingCommandTemplate == null) {
            throw new IllegalStateException(commandProp + " property is not defined");
        }

        if (!pingCommandTemplate.contains("${host}")) {
            throw new IllegalStateException(commandProp + "property doesn't contain ${host} parameter");
        }
        return pingCommandTemplate;
    }

}
