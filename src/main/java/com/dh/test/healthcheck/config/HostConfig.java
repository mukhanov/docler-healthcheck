package com.dh.test.healthcheck.config;

/**
 * @author mukhanov@gmail.com
 */
public class HostConfig {

    public final String host;

    public final String reportServerURL;

    public final String pingCommand;
    public final long pingDelayMillis;

    public final String traceRouteCommand;
    public final long traceRouteDelayMillis;

    public final String tcpPingURL;
    public final long tcpPingDelayMillis;

    public HostConfig(String host, String reportServerURL, String tcpPingURL, long tcpPingDelayMillis, String pingCommand, long pingDelayMillis, String traceRouteCommand, long traceRouteDelayMillis) {
        this.reportServerURL = reportServerURL;
        this.tcpPingURL = tcpPingURL;
        this.tcpPingDelayMillis = tcpPingDelayMillis;
        this.pingDelayMillis = pingDelayMillis;
        this.traceRouteCommand = traceRouteCommand;
        this.pingCommand = pingCommand;
        this.host = host;
        this.traceRouteDelayMillis = traceRouteDelayMillis;
    }

    @Override
    public String toString() {
        return "Config{" +
                "host='" + host + '\'' +
                ", reportServerURL='" + reportServerURL + '\'' +
                ", pingCommand='" + pingCommand + '\'' +
                ", pingDelayMillis=" + pingDelayMillis +
                ", traceRouteCommand='" + traceRouteCommand + '\'' +
                ", traceRouteDelayMillis=" + traceRouteDelayMillis +
                ", tcpPingURL='" + tcpPingURL + '\'' +
                ", tcpPingDelayMillis=" + tcpPingDelayMillis +
                '}';
    }
}
