package com.dh.test.healthcheck.commands;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author mukhanov@gmail.com
 */
abstract class AbstractHealthCheckCommand implements Runnable {

    protected final Logger log;
    protected final String host;
    private final String reportServerURL;
    private final Map<String, CommandResult> commandResultMap;

    public AbstractHealthCheckCommand(Logger log, String host, Map<String, CommandResult> commandResultMap, String reportServerURL) {
        this.log = log;
        this.host = host;
        this.commandResultMap = commandResultMap;
        this.reportServerURL = reportServerURL;
    }


    protected String readString(InputStream inputStream) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        String line;
        StringBuilder output = new StringBuilder();
        while ((line = reader.readLine()) != null) {
            output.append(line).append("\n");
        }

        return output.toString();
    }

    protected void setResult(CommandResult commandResult) {
        commandResultMap.put(commandResult.host, commandResult);
    }

    protected void reportResults() {
        try {
            URL url = new URL(reportServerURL);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            con.setDoOutput(true);
            try (DataOutputStream dataOutputStream = new DataOutputStream(con.getOutputStream())) {
                String json = serializeStateToJSON();
                dataOutputStream.writeChars(json);
                dataOutputStream.flush();
            }
            if (con.getResponseCode() != 200) {
                String responseString = readString(con.getInputStream());
                log.log(Level.WARNING, "Error sending state to service:\n" + responseString);
            }
        } catch (IOException e) {
            log.log(Level.WARNING, e.getMessage(), e);
        }
    }

    private String serializeStateToJSON() {
        StringBuilder jsonString = new StringBuilder("{\"" + host + "\":{");
        commandResultMap.entrySet().forEach(entry -> jsonString
                .append("\"").append(entry.getKey()).append("\"")
                .append(":{\"result\":")
                .append(entry.getValue().toJSON())
                .append("},"));

        jsonString.setLength(jsonString.length() - 1);

        return jsonString.append("}}").toString();
    }

}
