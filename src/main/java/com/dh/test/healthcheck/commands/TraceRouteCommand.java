package com.dh.test.healthcheck.commands;

import com.dh.test.healthcheck.config.HostConfig;

import java.util.Map;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author mukhanov@gmail.com
 */
public class TraceRouteCommand extends AbstractHealthCheckShellCommand {

    private final static Pattern pattern = Pattern.compile("\\d+ \\* \\* \\*", Pattern.MULTILINE);

    public TraceRouteCommand(HostConfig hostConfig, Map<String, CommandResult> commandStateMap) {
        super(Logger.getLogger("traceroute-" + hostConfig.host), hostConfig.traceRouteCommand, hostConfig.host, commandStateMap, hostConfig.reportServerURL);
    }

    protected boolean haveProblem(String outStr) {

        String lastLine = outStr.substring(outStr.trim().lastIndexOf("\n"));

        Matcher matcher = pattern.matcher(lastLine);

        return matcher.matches();
    }
}
