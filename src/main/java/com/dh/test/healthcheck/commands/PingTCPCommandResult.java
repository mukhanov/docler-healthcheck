package com.dh.test.healthcheck.commands;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author mukhanov@gmail.com
 */
public class PingTCPCommandResult extends CommandResult {

    protected final long responseTime;

    public PingTCPCommandResult(String host, String output, LocalDateTime timestamp, long responseTime) {
        super(host, output, timestamp);
        this.responseTime = responseTime;
    }


    public String toJSON() {

        return "{\"output\":" + quote(output) +
                ", \"timestamp\":\"" + DateTimeFormatter.ISO_DATE_TIME.format(timestamp) +
                "\", \"responseTime\":" + responseTime +
                "}";
    }


}
