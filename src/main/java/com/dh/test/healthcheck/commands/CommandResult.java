package com.dh.test.healthcheck.commands;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author mukhanov@gmail.com
 */
public class CommandResult {
    protected final String host;
    protected final String output;
    protected final LocalDateTime timestamp;

    public CommandResult(String host, String output, LocalDateTime timestamp) {
        this.host = host;
        this.output = output;
        this.timestamp = timestamp;
    }

    protected String quote(String string) {
        if (string == null || string.length() == 0) {
            return "\"\"";
        }

        StringBuilder sb = new StringBuilder(string.length() + 4).append('"');

        string.chars().forEach(i -> {
            char c = (char) i;
            switch (c) {
                case '\\':
                case '"':
                    sb.append('\\');
                    sb.append(c);
                    break;
                case '/':
                    sb.append('\\').append(c);
                    break;
                case '\b':
                    sb.append("\\b");
                    break;
                case '\t':
                    sb.append("\\t");
                    break;
                case '\n':
                    sb.append("\\n");
                    break;
                case '\f':
                    sb.append("\\f");
                    break;
                case '\r':
                    sb.append("\\r");
                    break;
                default:
                    if (c < ' ') {
                        String t = "000" + Integer.toHexString(c);
                        sb.append("\\u").append(t.substring(t.length() - 4));
                    } else {
                        sb.append(c);
                    }
            }
        });
        return sb.append('"').toString();
    }

    public String toJSON() {

        return "{\"output\":" + quote(output) + ", \"timestamp\":\"" + DateTimeFormatter.ISO_DATE_TIME.format(timestamp) + "\"}";
    }


}
