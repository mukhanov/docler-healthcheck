package com.dh.test.healthcheck.commands;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author mukhanov@gmail.com
 */
public abstract class AbstractHealthCheckShellCommand extends AbstractHealthCheckCommand {

    protected final String command;

    public AbstractHealthCheckShellCommand(Logger log, String command, String host, Map<String, CommandResult> commandResultMap, String reportServerURL) {
        super(log, host, commandResultMap, reportServerURL);
        this.command = command;
    }

    protected abstract boolean haveProblem(String outStr);

    public void run() {
        try {
            Process exec = Runtime.getRuntime().exec(command);

            if (exec.waitFor() != 0) {
                log.warning("Abnormal ping termination: " + exec.exitValue());
            }

            String outStr = readString(exec.getInputStream());
            if ("".equals(outStr)) {
                outStr = readString(exec.getErrorStream());
            }

            setResult(new CommandResult(host, outStr, LocalDateTime.now()));

            if (haveProblem(outStr)) {
                reportResults();
            }

        } catch (Throwable e) {
            log.log(Level.WARNING, e.getMessage(), e);
        }
    }

}
