package com.dh.test.healthcheck.commands;

import com.dh.test.healthcheck.config.HostConfig;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author mukhanov@gmail.com
 */
public class PingTCPCommand extends AbstractHealthCheckCommand {

    private final String tcpPingURL;

    public PingTCPCommand(HostConfig hostConfig, Map<String, CommandResult> commandStateMap) {
        super(Logger.getLogger("ping-tcp-" + hostConfig.host), hostConfig.host, commandStateMap, hostConfig.reportServerURL);
        this.tcpPingURL = hostConfig.tcpPingURL;
    }

    @Override
    public void run() {

        try {
            URL url = new URL(tcpPingURL);

            long startMillis = System.currentTimeMillis();

            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

            try {

                try (InputStream inputStream = httpURLConnection.getInputStream()) {

                    String outStr = readString(inputStream);

                    setResult(new PingTCPCommandResult(host, outStr, LocalDateTime.now(), System.currentTimeMillis() - startMillis));

                    if (httpURLConnection.getResponseCode() != 200) {
                        reportResults();
                    }
                }
            } finally {
                httpURLConnection.disconnect();
            }

        } catch (IOException e) {
            log.log(Level.WARNING, e.getMessage(), e);
            StringWriter stringWriter = new StringWriter();
            e.printStackTrace(new PrintWriter(stringWriter));
            setResult(new CommandResult(host, stringWriter.toString(), LocalDateTime.now()));
            reportResults();
        } catch (Throwable e) {
            log.log(Level.WARNING, e.getMessage(), e);
        }
    }
}
