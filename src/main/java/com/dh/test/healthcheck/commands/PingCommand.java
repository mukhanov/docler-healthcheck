package com.dh.test.healthcheck.commands;

import com.dh.test.healthcheck.config.HostConfig;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author mukhanov@gmail.com
 */
public class PingCommand extends AbstractHealthCheckShellCommand {

    private final static Pattern pattern = Pattern.compile(".*---(\\d*) packets transmitted, (\\d*) packets received.*", Pattern.MULTILINE);

    public PingCommand(HostConfig hostConfig, Map<String, CommandResult> commandStateMap) {
        super(Logger.getLogger("ping-" + hostConfig.host), hostConfig.pingCommand, hostConfig.host, commandStateMap, hostConfig.reportServerURL);
    }


    protected boolean haveProblem(String outStr) {
        Matcher matcher = pattern.matcher(outStr.replaceAll("\n", ""));

        if (!matcher.matches()) {
            log.log(Level.WARNING, outStr);
            return true;
        }
        String transmittedStr = matcher.group(1);
        String receivedStr = matcher.group(2);

        int transmitted = Integer.valueOf(transmittedStr);
        int received = Integer.valueOf(receivedStr);

        if (transmitted != received) {
            log.log(Level.WARNING, outStr);
            return true;
        }

        return false;
    }
}
